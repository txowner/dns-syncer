build:
	go build -o bin/syncer *.go

start:
	./bin/syncer

start-cron:
	./bin/syncer --mode cron

run:
	go run *.go