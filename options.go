package main

import "flag"

const (
	modeOnce = "once"
	modeCron = "cron"
)

var (
	channel = flag.String("channel", "aliyun", "domain update channel")
	mode    = flag.String("mode", modeOnce, "if you want run with cronjob, set it to cron")
)
