package aliyun

import "xtsec.com/dns-syncer/common"

type aliyunDomainRecordRequest struct {
	RecordId *string
	RR       *string
	Type     *string
	TTL      *int64
	Value    string // 调用时动态获取
}

func NewAliyunDomainRecordRequestList() []aliyunDomainRecordRequest {
	records := common.GetSetting().DomainRecords
	result := make([]aliyunDomainRecordRequest, 0, len(records))
	for _, c := range records {
		for _, r := range c.Records {
			result = append(result, aliyunDomainRecordRequest{
				RecordId: r.ID, // 在网站上修改时查看网络请求可获取 对应到域名 832424421522692096
				RR:       r.RR, // 主机记录  test
				TTL:      r.TTL,
				Type:     r.Type, //  A记录
			})
		}
	}
	return result
}
