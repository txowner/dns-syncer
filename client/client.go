package client

import (
	"fmt"

	"xtsec.com/dns-syncer/client/aliyun"
)

type Interface interface {
	Name() string
	Start(string) (bool, error) // updated, error
}

func GetClient(channel string) (Interface, error) {
	switch channel {
	case aliyun.Aliyun:
		configList := aliyun.NewAliyunDomainRecordRequestList()
		return aliyun.NewClient(configList), nil
	default:
		return nil, fmt.Errorf("can't get client for channel: %v", channel)
	}
}
