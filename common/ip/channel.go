// check the channel is valid
package ip

// 1. 启动时获取全部ip
// 2. 将不能用的channel放进忽略通道
// 3. 周期性检查全部 将不能用的channel放进忽略通道
// 4. 新增不能访问的channel，发送邮件通知

func ipScore(ipMap map[string]string) map[string]int {
	result := make(map[string]int)
	for _, ip := range ipMap {
		score, ok := result[ip]
		if ok {
			result[ip] = score + 1
			continue
		}
		result[ip] = 1
	}

	return result
}
