package ip

import (
	"fmt"
	"testing"
)

func TestGetAll(t *testing.T) {
	ips, err := GetAll()
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(ips)
}
