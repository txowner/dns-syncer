package pool

const ipifyLookup = "https://api.ipify.org/?format=json"

type ipify struct{ base }

func NewIpify() *ipify {
	return &ipify{base: base{lookup: ipifyLookup, target: "ipify"}}
}
