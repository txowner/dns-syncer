package pool

import (
	"context"

	"xtsec.com/dns-syncer/common/tools"
)

const httpBinLookup = "http://httpbin.org/get"

type httpBin struct{ base }

type httpBinGetResponse struct {
	Args    map[string]interface{} `json:"args"`
	Headers map[string]string      `json:"headers"`
	Origin  string                 `json:"origin" validate:"ip"`
	Url     string                 `json:"url"`
}

func NewHttpBin() *httpBin {
	return &httpBin{base: base{lookup: httpBinLookup, target: "httpBin"}}
}

func (httpBin httpBin) GetIPResponse(ctx context.Context) (IPResponse, error) {
	ir := IPResponse{Target: httpBin.target}

	var resp httpBinGetResponse

	err := httpBin.client().UserAgent(tools.RandChoice[string](uaList)).ToJSON(&resp).Fetch(ctx)
	if err != nil {
		return ir, err
	}

	if err := tools.ValidateStruct(resp); err != nil {
		return ir, err
	}

	ir.Ip = resp.Origin

	return ir, nil
}
