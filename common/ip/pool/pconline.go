package pool

const pconlineLookup = "http://whois.pconline.com.cn/ipJson.jsp?json=true"

type pconline struct{ base }

func NewPconline() *pconline {
	return &pconline{base: base{lookup: pconlineLookup, target: "pconline"}}
}
