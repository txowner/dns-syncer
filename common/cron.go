package common

import (
	"github.com/robfig/cron/v3"
)

type taskHandle func()

func CronTask(task taskHandle) *cron.Cron {
	c := cron.New(cron.WithSeconds())

	c.AddFunc("0 */2 * * * *", task) // 秒 分 时 日 月 周
	return c
}
