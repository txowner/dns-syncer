package tools

import (
	"fmt"
	"testing"
)

func TestGetProjectBaseDir(t *testing.T) {
	projectDir, err := GetProjectBaseDir("dns-syncer")
	if err != nil {
		t.Errorf("GetProjectBaseDir failed: %v", err)
	}
	fmt.Println("GetProjectBaseDir success: ", projectDir)
}
