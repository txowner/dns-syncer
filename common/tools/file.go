package tools

import (
	"os"
	"strings"
)

func WriteFile(filename string, data string) error {
	// 写入文件
	return os.WriteFile(filename, []byte(data), 0644)
}

func ReadFile(filename string, ignoreNotExist bool) (string, error) {
	// 写入文件
	data, err := os.ReadFile(filename)
	if err != nil {
		if ignoreNotExist && strings.Contains(err.Error(), "no such file or directory") {
			return "", nil
		}

		return "", err
	}

	return string(data), nil
}
