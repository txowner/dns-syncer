package tools

import (
	"fmt"
	"testing"
)

func TestXxx(t *testing.T) {
	filename := "ip.txt"
	ip := "127.0.0.1"

	if err := WriteFile(filename, ip); err != nil {
		t.Fatal(err)
	}

	data, err := ReadFile(filename, false)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(data == ip)
}
