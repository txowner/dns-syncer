package tools

import "math/rand"

func RandInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func RandChoice[T any](ts []T) T {
	index := rand.Intn(len(ts))
	return ts[index]
}

func RandIndex[T any](ts []T) int {
	return rand.Intn(len(ts))
}
