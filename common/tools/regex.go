package tools

import (
	"regexp"
)

const regIpStr = "((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}"

func MatchIp(text string) (ip string, err error) {
	reg, err := regexp.Compile(regIpStr) //编译正则表达式 当前场景不需要提前编译存放全局
	if err != nil {
		return "", err
	}

	ipBytes := reg.Find([]byte(text))

	ip = string(ipBytes)
	return
}
