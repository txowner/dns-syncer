package email

import (
	"fmt"
	"testing"
)

func TestEmail(t *testing.T) {
	email := NewEmail("smtp.qq.com", 587, "xxx@qq.com", "xxx", "xxx@qq.com")
	if err := email.Send("测试邮件", "当前ip是: 127.0.0.1"); err != nil {
		t.Fatal(err)
	}

	fmt.Println("邮件发送成功")
}
