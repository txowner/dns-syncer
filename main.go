package main

import (
	"flag"
	"fmt"

	"xtsec.com/dns-syncer/client"
	"xtsec.com/dns-syncer/common"
	"xtsec.com/dns-syncer/flow"
)

func main() {
	flag.Parse()

	fmt.Printf("[%s] start sync %s dns record...\n\n", *mode, *channel)

	dnsSyncClient, err := client.GetClient(*channel)
	if err != nil {
		fmt.Printf("get sync client %s error:  %v\n", *channel, err)
		return
	}

	switch *mode {
	case modeOnce:
		flow.Sync(dnsSyncClient)
	case modeCron:
		c := common.CronTask(func() {
			flow.Sync(dnsSyncClient)
		})
		c.Run()
	default:
		fmt.Println("not support sync mode.")
		return
	}

	fmt.Printf("[%s]sync dns record job success exit\n", *mode)
}
